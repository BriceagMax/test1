<?
     
$dataBook = "books.json";

 
 
    if ( !empty($_POST)) {
        // keep track validation errors
        $idError = null;
        $nameError = null;
        $autorError = null;
    $yearError = null;
         
        // keep track post values
    $id = $_POST['id'];
        $name = $_POST['name'];
        $autor = $_POST['autor'];
        $year = $_POST['year'];
         
        // validate input
        $valid = true;
     if (empty($id)) {
            $idError = 'Please enter Id';
            $id = false;
        }
    
        if (empty($name)) {
            $nameError = 'Please enter Name';
            $valid = false;
        }
         
        if (empty($autor)) {
            $autorError = 'Please enter Autor';
            $valid = false;
        } 
    
        if (empty($year)) {
            $yearError = 'Please enter Year';
            $valid = false;
        }
         
        // insert data
        if ($valid) { 
          $getData = file_get_contents($dataBook);
          $books = json_decode($getData, true)['books'];
          $added = [
            "id" => end($books)['id'] + 1,
            "name" => trim($_POST['name']),
            "autor" => trim($_POST['autor']),
            "year" => trim($_POST['year']),
    ];
    $books[] = $added;
    file_put_contents($dataBook, json_encode(['books' => $books]));
  }
 }
 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>Add Book</h3>
                    </div>
             
                    <form class="form-horizontal" action="add.php" method="post">
					 <div class="control-group <?php echo !empty($idError)?'error':'';?>">
                        <label class="control-label">Id</label>
                        <div class="controls">
                            <input name="id" type="text"  placeholder="Id" value="<?php echo !empty($id)?$id:'';?>">
                            <?php if (!empty($idError)): ?>
                                <span class="help-inline"><?php echo $idError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
                            <?php if (!empty($nameError)): ?>
                                <span class="help-inline"><?php echo $nameError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($autorError)?'error':'';?>">
                        <label class="control-label">Autor</label>
                        <div class="controls">
                            <input name="autor" type="text" placeholder="Autor" value="<?php echo !empty($autor)?$autor:'';?>">
                            <?php if (!empty($autorError)): ?>
                                <span class="help-inline"><?php echo $autorError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($yearError)?'error':'';?>">
                        <label class="control-label">Year</label>
                        <div class="controls">
                            <input name="year" type="text"  placeholder="Year" value="<?php echo !empty($year)?$year:'';?>">
                            <?php if (!empty($yearError)): ?>
                                <span class="help-inline"><?php echo $yearError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
					  <br>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">Add</button>
                          <a class="btn btn-danger" href="index.php">Back</a>
                        </div>
                    </form>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>