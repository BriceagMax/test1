<?
$dataBook = 'books.json';
$getData = file_get_contents($dataBook);
$books = json_decode($getData, true)['books'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
 
<body>
    <div class="container">
            <div class="row">
                <h3>Books_json</h3>
            </div>
            <div class="row">
            	<p>
                    <a href="add.php" class="btn btn-info">Create</a> 
                </p>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Autor</th>
			<th>Year</th> 
        </tr>
    </thead>

    <tbody>
        <?php 
            foreach ($books as $row) 
            {
                echo '<tr>';
                echo '<td>'. $row['id'] . '</td>';
                echo '<td>'. $row['name'] . '</td>';
                echo '<td>'. $row['autor'] . '</td>';
				echo '<td>'. $row['year'] . '</td>';
                echo '</td>';
                echo '</tr>';
            }  
        ?>
    </tbody>
</table>
</div>
</div>
</body>
</html>

